//
//  AppViewController.swift
//  AwesomeProjectFramework
//
//  Created by cheungpat on 08/06/2022.
//

import UIKit

// Without implementationOnly, Xcode will complain that it cannot find React
// when building. This hides react dependency behind our framework.e
// https://github.com/Carthage/Carthage/issues/2618#issuecomment-672074666
@_implementationOnly import React

public class AppViewController: UIViewController {
  public override func loadView() {
    NSLog("Hello")
    
//    NSBundle(
//    NSBundle(for: <#T##AnyClass#>)
//    let jsCodeLocation = NSBundle(URLForResource: "main", withExtension:"jsbundle");
          let bundle = Bundle(for: Self.self);
          let url = bundle.url(forResource: "main", withExtension: "jsbundle");

    let mockData:NSDictionary = ["scores":
        [
            ["name":"Alex", "value":"42"],
            ["name":"Joel", "value":"10"]
        ]
    ]

    // Should use bridge instead so that react screens can share a VM. But this requires we have
    // a singleton for the bridge. Left as an exercise.
//    let bridge = RCTBridge(delegate: self, launchOptions: nil);
//    let rootView = RCTRootView(bridge: bridge!, moduleName: "AwesomeProject", initialProperties: mockData as! [AnyHashable : Any]);
    
    // Make sure the component is registered:
    let rootView = RCTRootView(bundleURL: url!, moduleName: "AwesomeProject", initialProperties: mockData as! [AnyHashable : Any]);
//    let vc = UIViewController()
    self.view = rootView

  }

  public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}


//extension AppViewController: RCTBridgeDelegate {
//  public func sourceURL(for bridge: RCTBridge!) -> URL! {
//      let bundle = Bundle(for: Self.self);
//      return bundle.url(forResource: "main", withExtension: "jsbundle");
//
//    }
//
//
//}
