//
//  ViewController.swift
//  AwesomeCallerProject
//
//  Created by cheungpat on 08/06/2022.
//

import UIKit
import AwesomeProjectFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func click(_ sender: Any) {
        let vc = AppViewController();
        self.present(vc, animated: true);
    }
    
}

