# Embed React.framework demo

What I did:

1. Create React App.
1. Add a framework target.
1. Add the target to Podfile, run pod install
1. Add certain required frameworks into target. These frameworks are also found
   dynamically linked into a react native app.
1. Set these frameworks as embed.
1. Add a build script phase to build react native js bundle.
1. Create a new app project inside workspace. It does not need Podfile.
1. In Xcode, drag the framework output into the target build settings.
1. If the new app project uses scene delegate, add a window variable to the app
   delegate so that react can find the window.
1. Build.

